# Getting Started With a(8)

After completing the installation and the Quick Start below, you will be able to deploy any function you'd like to the a8 platform. Please let us know of an issues by opening an issue in the appropriate repository. [Join our slack group](https://a8-slack.herokuapp.com) if you have any questions or want to get involved with our developer community.

## Installing a8

### Pre-requisites

1. [Install docker](https://docs.docker.com/v17.12/install/)

### Install the correct CLI binary

For linux and MacOs

`curl -LSs https://gitlab.com/autom8.network/go-a8-cli/raw/master/install | sh`

Sorry Windows, we're not there yet.

### Run the daemon

1. On the command line run

    `docker run -d --rm --name daemon -p 3035:3035 autom8network/node:0.0.8 a8-node  --http-port 3035 --libp2p-port 4004 --bootstrappers /ip4/3.209.172.134/tcp/4002/ipfs/Qmeq45rCLjFt573aFKgLrcAmAMSmYy9WXTuetDsELM2r8m`

    This will automatically pull the latest daemon docker image, run it, and connect to the a8 network.

1. Test that the daemon can talk to the a8 network by running `a8 show ns`. If you see a list of apps instead of an error, it's party time.

### Known Issues

All is right in the world... ✅

<br/>

## Quick Start

### Create account

Before getting started with autom(8) you need to create an account. Fortunately this is very easy to do!

``` bash
a8 create account --email <youremail@geocities.com> --handle <yourhandle>
```

⛑  <yourhandle> must only use lowercase alphanumerics and _. Further, it must be between 2 and 30 characters (no but seriously, much less than 30 characters).

Make sure you choose your handle carefully, this will be the root name of all of your functions.

### Creating function scaffolding

For now, go to your favorite directory and create an app directory called `functions`

The first thing you'll want to do is create a function scaffolding for your new function. This process initializes a directory with a hello world function in the specified runtime. In your function directory, run the following command

`a8 init <funciton name>`

If you're planning on use this function in Slack or Discord include --slack and/or --discord, respectively.
These options will add functions to the boilerplate that will make it easy to format your function's output for Slack and Discord.
  
⛑  <function name> must only use lowercase alphanumerics and underscores. 
Further, it must be between 2 and 30 characters (seriously though, why would you use anything remotely close to 30 characters!?).

### Code your function

We initialized a function using the node runtime, so let's have a look at func.js

``` javascript
const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  let name = 'World';
  if (input.name) {
    name = input.name;
  }
  return {'message': 'Hello ' + name}
})
```

Pretty straightforward. All you need to do is change the handle function to implement your desired functionality. For now, though, let's leave it as is and do a deploy.

### Deploy your function

To deploy your function, simply issue the following command:

`a8 deploy`

No errors? Then you just deployed your first function to the autom(8) network. Groovy! 🕺

### Invoke your function

Now that you've deployed your function, let's give it a test drive. One thing you'll need to know is your function's fully qualified name, which is simply `your-app-name.function-name`. Once you have that, simply issue the command:

`a8 invoke your-app-name.helloWorld`

This will return the following output:

``` json
{"message":"Hello World"}
```

How about inputs? Simple. Issue the following command:

`a8 invoke your-app-name.helloWorld '{"name":"Elmo"}'`

This will return the following output:

``` json
{"message":"Hello Elmo"}
```

Easy, right? Now it's off the the races!! 🏁🏁🏁🏁🏁🏁

## Next Steps

- Learn about building for our Slack bot [here](./slack_bot.md)
- Learn about building for our Discord bot [here](./discord_bot.md)
- Todo: custom docker image configurations